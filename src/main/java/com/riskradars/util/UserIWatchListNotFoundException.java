package com.riskradars.util;

@SuppressWarnings("serial")
public class UserIWatchListNotFoundException extends RuntimeException {

    public UserIWatchListNotFoundException(Long id) {
      super("Could not find user watchlist " + id);
    }
  }