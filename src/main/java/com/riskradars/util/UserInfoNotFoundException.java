package com.riskradars.util;

@SuppressWarnings("serial")
public class UserInfoNotFoundException extends RuntimeException {

    public UserInfoNotFoundException(Long id) {
      super("Could not find userinfo " + id);
    }
  }