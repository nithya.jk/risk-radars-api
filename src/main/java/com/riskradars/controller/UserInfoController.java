package com.riskradars.controller;

import com.riskradars.application.UserInfoRepository;
import com.riskradars.modal.Userinformation;
import com.riskradars.util.UserInfoNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders ="*")
@RestController
@RequestMapping("/userinfo")
public class UserInfoController {

    private final UserInfoRepository userInfoRepository;

    public UserInfoController(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Userinformation createUserInfo(@RequestBody Userinformation userInfo) {
        return userInfoRepository.save(userInfo);
    }

    @GetMapping("/get/all")
    public Iterable<Userinformation> getUserInfo() {
        return userInfoRepository.findAll();
    }

    @GetMapping("/get/userid/{userId}")
    public Userinformation getUserInfoById(@PathVariable Long userId) {
        return userInfoRepository.findById(userId)
            .orElseThrow(() -> new UserInfoNotFoundException(userId));
    }

    @GetMapping("/get/username/{username}")
    public Userinformation getUserInfoByUserName(@PathVariable String username) {
        return userInfoRepository.findByUsername(username);
    }

    @GetMapping("/get/{username}/{authkey}")
    public boolean isValidUser(@PathVariable String username, @PathVariable String authkey) {
        Userinformation userinformation = userInfoRepository.findByUsernameAndAuthkey(username, authkey);
        if(userinformation != null)
        {
            return true;
        }
        return false;
    }

    @GetMapping("/get/isvalid/username/{username}")
    public boolean isValidUserName(@PathVariable String username) {
        Userinformation userinformation = userInfoRepository.findByUsername(username);
        if(userinformation != null)
        {
            return false;
        }
        return true;
    }

    @GetMapping("/test")
    public String getTest() {
        return "Test userinfo Successful";
    }

    @DeleteMapping("/delete/{username}")
    public boolean deleteUserInfoById(@PathVariable String username) {
        Userinformation userinformation = userInfoRepository.findByUsername(username);
        if(userinformation != null)
        {
            userInfoRepository.deleteById(userinformation.getUserid());
            return true;
        }
        return false;
    }
}