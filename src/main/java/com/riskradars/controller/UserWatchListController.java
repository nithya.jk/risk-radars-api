package com.riskradars.controller;

import java.util.stream.StreamSupport;

import com.riskradars.application.UserWatchListRepository;
import com.riskradars.modal.Userwatchlistinfo;
import com.riskradars.util.UserInfoNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders ="*")
@RestController
@RequestMapping("/userwatchlist")
public class UserWatchListController {

    private final UserWatchListRepository userWatchListRepository;

    public UserWatchListController(UserWatchListRepository userWatchListRepository) {
        this.userWatchListRepository = userWatchListRepository;
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Userwatchlistinfo createUserWatchList(@RequestBody Userwatchlistinfo userwatchlist) {
        return userWatchListRepository.save(userwatchlist);
    }

    @GetMapping("/get/all")
    public Iterable<Userwatchlistinfo> getAllUserWatchList() {
        return userWatchListRepository.findAll();
    }

    @GetMapping("/get/{username}")
    public Iterable<Userwatchlistinfo> getUserWatchList(@PathVariable String username) {
        return userWatchListRepository.findByUsername(username);
    }

    @GetMapping("/test")
    public String getTest() {
        return "Test user watchlist Successful";
    }

    @DeleteMapping("/delete/{username}/{symbol}")
    public boolean deleteUserInfoById(@PathVariable String username, @PathVariable String symbol) {
        Userwatchlistinfo userwatchlistinfo = userWatchListRepository.findByUsernameAndSymbol(username, symbol);
        if(userwatchlistinfo != null)
        {
            userWatchListRepository.deleteById(userwatchlistinfo.getUserwatchlistid());
            return true;
        }
        return false;
    }
}