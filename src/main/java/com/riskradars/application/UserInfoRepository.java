package com.riskradars.application;

import com.riskradars.modal.Userinformation;

import org.springframework.data.repository.CrudRepository;

public interface UserInfoRepository extends CrudRepository<Userinformation, Long> {
    Userinformation findByUsername(String username);

    Userinformation findByUsernameAndAuthkey(String username, String authkey);
}