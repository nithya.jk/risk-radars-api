package com.riskradars.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.riskradars.controller", "com.riskradars.controller"})
public class RiskRadarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiskRadarsApplication.class, args);
	}

}
