package com.riskradars.application;

import com.riskradars.modal.Userwatchlistinfo;

import org.springframework.data.repository.CrudRepository;

public interface UserWatchListRepository extends CrudRepository<Userwatchlistinfo, Long> {
    Userwatchlistinfo findByUsernameAndSymbol(String username, String symbol);

    Iterable<Userwatchlistinfo> findByUsername(String username);
}