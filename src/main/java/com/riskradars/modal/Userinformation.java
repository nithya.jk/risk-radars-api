package com.riskradars.modal;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Userinformation {

    public Userinformation() {
    }

    public Userinformation(String firstname, String lastname, String email, Date dateofbirth, String username, String authkey) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.dateofbirth = dateofbirth;
        this.username = username;
        this.authkey = authkey;
    }

    @Id
    private Long userid;

    private String firstname;

    private String lastname;

    private String email;

    private Date dateofbirth;

    private String username;
    
    private String authkey;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(Date dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthkey() {
        return authkey;
    }

    public void setAuthkey(String authkey) {
        this.authkey = authkey;
    }
}