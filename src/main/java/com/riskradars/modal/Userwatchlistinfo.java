package com.riskradars.modal;

import org.springframework.data.annotation.Id;

public class Userwatchlistinfo {

    public Userwatchlistinfo() {
    }

    public Userwatchlistinfo(String username, String symbol, int risk, String stockname, String industry) {
        this.username = username;
        this.symbol = symbol;
        this.risk = risk;
        this.stockname = stockname;
        this.industry = industry;
    }

    @Id
    private Long userwatchlistid ;

    private String username;

    private String symbol;

    private String stockname;

    private String industry;

    private int risk;

    public Long getUserwatchlistid() {
        return userwatchlistid;
    }

    public void setUserwatchlistid(Long userwatchlistid) {
        this.userwatchlistid = userwatchlistid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getStockname() {
        return stockname;
    }

    public void setStockname(String stockname) {
        this.stockname = stockname;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }
}